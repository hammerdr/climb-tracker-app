import UIKit

class ClimbTypeCollectionViewCell: UICollectionViewCell {
    @IBOutlet var image: UIImageView!
    @IBOutlet var check: UIImageView!
    @IBOutlet var name: UILabel!
}