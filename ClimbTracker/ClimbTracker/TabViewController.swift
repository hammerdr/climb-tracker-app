import UIKit

class TabViewController : UITabBarController {
    override func viewDidAppear(animated: Bool) {
        let tabBarItem = self.tabBar.items?[3]
        tabBarItem?.title = "Workout"
        let image = UIImage(named: "barbell-8x"),
        newSize = CGSize(width: 50, height: 50)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image?.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        tabBarItem?.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
}