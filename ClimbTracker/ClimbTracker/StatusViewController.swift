import UIKit

class StatusViewController: UIViewController {
    @IBOutlet weak var loading: UILabel!
    @IBOutlet weak var successMessage: UILabel!
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var errorImage: UIImageView!
    @IBOutlet weak var successImage: UIImageView!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        loading.hidden = false
        hideSuccess()
        hideError()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let url = NSURL(string: "http://climb-track.herokuapp.com/health_check")
        let request = NSURLRequest(URL: url!)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
            
            let result = JSON(data: data!)
            
            if(result["healthy"].asBool != nil && result["healthy"].asBool!) {
                
                self.successMessage.hidden = false
                self.successImage.hidden = false
                self.hideError()
                self.hideLoading()
            } else {
                
                self.errorMessage.hidden = false
                self.errorImage.hidden = false
                self.hideSuccess()
                self.hideLoading()
            }
        }
    }
    
    func hideError() {
        errorMessage.hidden = true
        errorImage.hidden = true
    }
    
    func hideLoading() {
        loading.hidden = true
    }
    
    func hideSuccess() {
        successMessage.hidden = true
        successImage.hidden = true
    }
}