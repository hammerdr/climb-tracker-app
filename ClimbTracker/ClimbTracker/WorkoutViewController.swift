import UIKit
import AVFoundation

class WorkoutViewController: UIViewController {
    @IBOutlet var timer : UILabel!
    @IBOutlet var start : UIButton!
    @IBOutlet var stop : UIButton!
    @IBOutlet var reset : UIButton!
    
    let TEN_MINUTES : Double = 60 * 10
    var _internalTimer : NSTimer? = nil
    var workout : Workout = Workout(seconds: 10 * 60)
    var soundPlayed = false
    
    override func viewDidAppear(animated: Bool) {
        timer.text = "9:59"
        stop.enabled = false
    }
    
    @IBAction func startTimer() {
        if(workout.stopped()) {
            workout.start()
            _internalTimer = NSTimer.scheduledTimerWithTimeInterval(
                1, target: self, selector: "tick", userInfo: nil, repeats: true)
            start.enabled = false
            reset.enabled = false
            stop.enabled = true
        }
    }
    
    @IBAction func stopTimer() {
        if(workout.started()) {
            workout.stop()
            _internalTimer?.invalidate()
            reset.enabled = true
            start.enabled = true
            stop.enabled = false
        }
    }
    
    @IBAction func resetTimer() {
        if(workout.stopped()) {
            workout = Workout(seconds: TEN_MINUTES)
            soundPlayed = false
            timer.text = "9:59"
            reset.enabled = false
            start.enabled = true
            stop.enabled = false
        }
    }
    
    func tick() {
        timer.text = String(format: "%d:%02d", workout.timeLeft() / 60, workout.timeLeft() % 60)
        if (workout.timeLeft() <= 0 && !soundPlayed) {
            if let soundURL = NSBundle.mainBundle().URLForResource("finished", withExtension: "wav") {
                var mySound: SystemSoundID = 0
                AudioServicesCreateSystemSoundID(soundURL, &mySound)
                AudioServicesPlaySystemSound(mySound);
                soundPlayed = true
            }
        }
    }
}