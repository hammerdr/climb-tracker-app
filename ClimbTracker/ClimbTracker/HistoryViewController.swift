import UIKit

class HistoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var historyTable: UITableView!
    
    var climbs = []
    
    override func viewDidAppear(animated: Bool) {
        reloadData()
    }
    
    func reloadData() {
        let url = NSURL(string: "http://climb-track.herokuapp.com/climbs")
        let request = NSMutableURLRequest(URL: url!)
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
            self.reloadDataFromData(data!)
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let row = self.climbs[indexPath.item]
        let cell = tableView.dequeueReusableCellWithIdentifier("history") as! HistoryTableViewCell
        cell.date.text = formattedDate(row["date"] as? String)
        cell.rating.text = row["rating"] as? String
        cell.climbType.text = row["climb_type"] as? String
        return cell
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            let climbId = climbs[indexPath.item]["id"] as! NSNumber
            let url = NSURL(string: "http://climb-track.herokuapp.com/climbs/" + climbId.description)
            print(url)
            let request = NSMutableURLRequest(URL: url!)
            request.HTTPMethod = "DELETE"
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
                self.reloadDataFromData(data!)
            }
        }
    }
    
    func reloadDataFromData(data: NSData) {
        do {
            self.climbs = try NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.AllowFragments)["climbs"] as! NSArray
            
        } catch {
            // Nothing for now
        }
        self.historyTable.reloadData()
    }
    
    func formattedDate(rawDate: String?) -> String {
        let inputFormat = NSDateFormatter()
        inputFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = inputFormat.dateFromString(rawDate!)
        let outputFormat = NSDateFormatter()
        outputFormat.dateFormat = "yyyy-MM-dd"
        return outputFormat.stringFromDate(date!)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return climbs.count
    }
}

