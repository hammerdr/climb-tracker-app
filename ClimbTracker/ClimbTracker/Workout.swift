import Foundation

class Workout {
    enum TimerState {
        case STOPPED
        case STARTED
    }
    
    var timeToEnd: NSDate = NSDate()
    var remainingTime: Int32
    var timerState: TimerState = TimerState.STOPPED
    
    init(seconds: Double) {
        remainingTime = Int32(seconds)
    }
    
    func start() {
        if (stopped()) {
            timerState = TimerState.STARTED
            timeToEnd = NSDate(timeIntervalSinceNow: Double(remainingTime))
        }
    }
    
    func stop() {
        if (started()) {
            timerState = TimerState.STOPPED
            remainingTime = timeLeft()
        }
    }
    
    func timeLeft() -> Int32 {
        return Int32(timeToEnd.timeIntervalSinceDate(NSDate()))
    }
    
    func stopped() -> Bool { return timerState == TimerState.STOPPED }
    func started() -> Bool { return timerState == TimerState.STARTED }
}