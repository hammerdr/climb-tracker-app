import UIKit

class HistoryTableViewCell: UITableViewCell {
    @IBOutlet var date: UILabel!
    @IBOutlet var rating: UILabel!
    @IBOutlet var climbType: UILabel!
}