import UIKit

class AddClimbViewController: UIViewController, UITextFieldDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var date: UITextField!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var toolbar: UIToolbar!
    @IBOutlet weak var ratingSlider: UISlider!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet var climbType : UICollectionView!
    @IBOutlet var statusBar : UIView!
    
    var selectedClimbType = 0
    let climbRating = ClimbRating()
    let CLIMB_TYPES : [Dictionary<String, AnyObject>] = [
        ["text": "Overhang", "image": UIImage(named: "noun_45246_cc")!],
        ["text": "Slab", "image": UIImage(named: "noun_45246_cc")!],
        ["text": "Vertical", "image": UIImage(named: "noun_45246_cc")!],
        ["text": "Crack", "image": UIImage(named: "noun_45246_cc")!],
        ["text": "Stemming", "image": UIImage(named: "noun_45246_cc")!]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateDate()
        datePicker.addTarget(self, action:"datePickerValueChanged:", forControlEvents:.ValueChanged)
        date.delegate = self
        let button = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: "dismissDatePicker:")
        toolbar.items = [button]
        date.inputView = datePicker
        date.inputAccessoryView = toolbar
        ratingSlider.maximumValue = climbRating.maximumRangeValue()
        ratingSlider.minimumValue = climbRating.minimumRangeValue()
        statusBar.alpha = 0
        ratingChanged(self);
    }
    
    func datePickerValueChanged(sender: AnyObject?) {
        updateDate();
    }
    
    func updateDate() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        date.text = dateFormatter.stringFromDate(datePicker.date)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        selectedClimbType = indexPath.item
        climbType.reloadData()
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return CLIMB_TYPES.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("climb", forIndexPath: indexPath) as! ClimbTypeCollectionViewCell
        
        cell.name.text = CLIMB_TYPES[indexPath.item]["text"] as? String
        cell.image.image = CLIMB_TYPES[indexPath.item]["image"] as? UIImage
        cell.check.image = UIImage(named: "check-8x")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        
        if (indexPath.item == selectedClimbType) {
            cell.layer.borderColor = UIColor.greenColor().CGColor
            cell.layer.borderWidth = 3.0
            cell.check.hidden = false
            cell.check.tintColor = UIColor.greenColor()
        } else {
            cell.layer.borderColor = UIColor.blackColor().CGColor
            cell.layer.borderWidth = 1.0
            cell.check.hidden = true
        }
        
        cell.layer.cornerRadius = 5.0
        return cell
    }
    
    @IBAction func ratingChanged(sender: AnyObject) {
        ratingLabel.text = climbRating.ratingFor(ratingSlider.value) as String
    }
    
    @IBAction func dismissDatePicker(sender: AnyObject) {
        date.endEditing(true)
    }
    
    @IBAction func save(sender: AnyObject) {
        date.endEditing(true)
        
        let data = [
            "climb": [
                "date": date.text!,
                "rating": climbRating.ratingFor(ratingSlider.value),
                "climb_type": CLIMB_TYPES[selectedClimbType]["text"] as! String
            ]
        ]
        
        let url = NSURL(string: "http://climb-track.herokuapp.com/climbs")
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        do {
            request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(data, options: [])
        } catch {
            request.HTTPBody = nil
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
            self.statusBar.alpha = 1
            UIView.animateWithDuration(3, animations: {
                self.statusBar.alpha = 0
            })
        }
    }
}

